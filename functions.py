# functions to symbolic diff:
# Class BinaryTree to make a parse tree
# Class Stack - LIFO Stack
# Functions to build parse tree and eval to differentiate

import operator as op


def sym_add(a, b):
    if is_formula(a) and is_formula(b):
        return a + " + " + b
    elif is_formula(a) and not is_formula(b):
        return a + " + " + str(b)
    elif is_formula(b) and not is_formula(a):
        return str(a) + " + " + b
    else:
        return int(a) + int(b)


def sym_mul(a, b):
    if is_number(a) and is_number(b):
        if is_int(a) and is_int(b):
            return int(a) * int(b)
        else:
            return float(a) * float(b)
    elif is_number(a) and is_variable(b):
        return str(a) + b
    elif is_variable(a) and is_number(b):
        return (b) + a
    elif is_variable(a) and is_variable(b):
        return a + b
    elif is_formula(a) and is_number(b):
        return str(b)+"(" + a + ")"
    elif is_number(a) and is_formula(b):
        return str(a) + "(" + b + ")"
    elif is_formula(a) and is_variable(b):
        return str(b)+"(" + a + ")"
    elif is_variable(a) and is_formula(b):
        return str(a) + "(" + b + ")"
    else:
        return "(" + a + ")" + "*" + "(" + b + ")"


# testing functions

def is_formula(x):
    try:
        _ = int(x)
        return False
    except ValueError:
        try:
            _ = float(x)
            return False
        except ValueError:
            _ = str(x)
            if _ == "":  return False
            return True


def is_variable(x):
    try:
        _ = int(x)
        return False
    except ValueError:
        try:
            _ = float(x)
            return False
        except ValueError:
            _ = str(x)
            if len(_) == 1:
                return True
            return False


def is_number(x):
    try:
        _ = int(x)
        return True
    except ValueError:
        try:
            _ = float(x)
            return True
        except ValueError:
            return False


def is_float(x):
    try:
        _ = float(x)
        return True
    except ValueError:
        return False


def is_int(x):
    try:
        _ = int(x)
        return True
    except ValueError:
        return False


def same_variable(a, b):
    if is_variable(a) and is_variable(b):
        return a == b




class Stack:
    def __init__(self):
        self.stack = []

    def push(self, x):
        self.stack.append(x)
        return True

    def pop(self):
        return self.stack.pop()

    def size(self):
        return len(self.stack)

    def is_empty(self):
        return False if self.stack else True

    def __str__(self):
        return str(self.stack)

    def peek(self):
        return self.stack[-1]

    def __eq__(self, other):
        return self.stack == other.stack


class BinaryTree:
    def __init__(self, rootObj):
        self.key = rootObj
        self.parent = None
        self.leftChild = None
        self.rightChild = None

    def insertLeft(self, newNode):
        if self.leftChild == None:
            t = BinaryTree(newNode)
            self.leftChild = t
            t.parent = self
            # self.leftChild = BinaryTree(newNode)
        else:
            t = BinaryTree(newNode)
            t.parent = self
            t.leftChild = self.leftChild
            self.leftChild.parent = t
            self.leftChild = t

    def insertRight(self, newNode):
        if self.rightChild is None:
            t = BinaryTree(newNode)
            self.rightChild = t
            t.parent = self
            # self.rightChild = BinaryTree(newNode)
        else:
            t = BinaryTree(newNode)
            t.parent = self
            t.rightChild = self.rightChild
            self.rightChild.parent = t
            self.rightChild = t

    def getRightChild(self):
        return self.rightChild

    def getLeftChild(self):
        return self.leftChild

    def getParent(self):
        return self.parent

    def setRootVal(self, obj):
        self.key = obj

    def getRootVal(self):
        return self.key


def inorder_traversal(tree):
    if tree:
        inorder_traversal(tree.getLeftChild())
        print(tree.getRootVal())
        inorder_traversal(tree.getRightChild())


def postorder_traversal(tree):
    if tree:
        postorder_traversal(tree.getLeftChild())
        postorder_traversal(tree.getRightChild())
        print(tree.getRootVal())


def leafs(tr):
    x = tr
    arr = []

    def helper(tree):
        if tree.getLeftChild():
            helper(tree.getLeftChild())
        if tree.getRightChild():
            helper(tree.getRightChild())
        if not tree.getLeftChild() and not tree.getRightChild():
            arr.append(tree.getRootVal())
    helper(x)
    return arr


def build_parse_tree(fpexp):
    fplist = fpexp.replace('(', ' ( ').replace(')', ' ) ').split()
    pStack = Stack()
    eTree = BinaryTree('')
    pStack.push(eTree)
    currentTree = eTree
    for i in fplist:
        if i == '(':
            currentTree.insertLeft('')
            pStack.push(currentTree)
            currentTree = currentTree.getLeftChild()
        elif i not in ['+', '-', '*', '/', ')']:
            currentTree.setRootVal(i)
            parent = pStack.pop()
            currentTree = parent
        elif i in ['+', '-', '*', '/']:
            currentTree.setRootVal(i)
            currentTree.insertRight('')
            pStack.push(currentTree)
            currentTree = currentTree.getRightChild()
        elif i == ')':
            currentTree = pStack.pop()
        else:
            raise ValueError
    return eTree


def evaluate(tree):
    opers = {'+': sym_add, '-': op.sub, '*': sym_mul, '/': op.truediv}

    leftT = tree.getLeftChild()
    rightT = tree.getRightChild()

    if leftT and rightT:
        fn = opers[tree.getRootVal()]
        return fn(evaluate(leftT), evaluate(rightT))
    else:
        return tree.getRootVal()


def evaluate_parse_tree(tree, var):
    opers = {'+': sym_add, '-': op.sub, '*': sym_mul, '/': op.truediv}

    leftT = tree.getLeftChild()
    rightT = tree.getRightChild()

    if leftT and rightT:
        fn = opers[tree.getRootVal()]
        # changes start:
        if fn is sym_mul:
            fn = sym_add
            return fn(sym_mul(evaluate(leftT), evaluate_parse_tree(rightT, var)), sym_mul(evaluate_parse_tree(leftT, var),
                    evaluate(rightT)))
        else:
            return fn(evaluate_parse_tree(leftT, var), evaluate_parse_tree(rightT, var))
    else:
        return derrive(tree.getRootVal(), var)


def derrive(exp, var):
    if is_number(exp): return 0
    elif is_variable(exp):
        return 1 if same_variable(exp, var) else 0




