# Symbolic Differentiation, simple version

# Types definitions:
from algos_data.algorithms.symbolic_diff.functions import *

if __name__ == '__main__':
   form = "((1 + (2 * x)) * (1 + x))"
   p_tree = BinaryTree('')
   p_tree = build_parse_tree(form)
   print(evaluate_parse_tree(p_tree, "x"))

